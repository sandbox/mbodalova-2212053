<?php
/**
 * @file
 * Create additional views handler.
 */

/**
 * Implements hook_views_data_alter().
 */
function more_dates_views_data_alter(&$data) {
  foreach ($data as $table_name => $table_info) {
    foreach ($table_info as $field_name => $field_info) {
      // Apply to date fields only.
      if (isset($field_info['field']['handler']) && $field_info['field']['handler'] == 'views_handler_field_date') {
        $data[$table_name][$field_name . '_more_dates'] = array(
          'title' => t('@label more_dates', array('@label' => $field_info['title'])),
          'help' => t('Adding settings for field date formats.'),
          'field' => array(
            'handler' => 'more_dates_views_handler_field_date',
            'click sortable' => TRUE,
          ),
          'real field' => $field_name,
        );
      }
    }
  }
  return $data;
}
