<?php

/**
 * @file
 * Definition of more_dates_views_handler_field_date.
 */

/**
 * A handler to provide proper displays for dates.
 *
 * @ingroup views_field_handlers
 */
class more_dates_views_handler_field_date extends views_handler_field_date {
  function option_definition() {
    $options = parent::option_definition();

    $options['more_dates']['dates']['past'] = array('default' => 'long');
    $options['more_dates']['dates']['yesterday'] = array('default' => 'long');
    $options['more_dates']['dates']['today'] = array('default' => 'long');
    $options['more_dates']['dates']['tomorrow'] = array('default' => 'long');
    $options['more_dates']['dates']['future'] = array('default' => 'long');
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $date_formats = array();
    $date_types = system_get_date_types();
    foreach ($date_types as $key => $value) {
      $date_formats[$value['type']] = t('@date_format format', array('@date_format' => $value['title'])) . ': ' . format_date(REQUEST_TIME, $value['type']);
    }

    $build = array(
      '#tree' => TRUE,
    );
    $build['past'] = array(
      '#type' => 'select',
      '#title' => t('Past'),
      '#options' => $date_formats,
      '#description' => t('The date format which will be used for field value until than day ago.'),
      '#default_value' => isset($this->options['more_dates']['dates']['past']) ? $this->options['more_dates']['dates']['past'] : 'long',
    );
    $build['yesterday'] = array(
      '#type' => 'select',
      '#title' => t('Yesterday'),
      '#options' => $date_formats,
      '#description' => t('The date format which will be used for field value equal yesterday.'),
      '#default_value' => isset($this->options['more_dates']['dates']['yesterday']) ? $this->options['more_dates']['dates']['yesterday'] : 'long',
    );
    $build['today'] = array(
      '#type' => 'select',
      '#title' => t('Today'),
      '#options' => $date_formats,
      '#description' => t('The date format which will be used for field value equal today.'),
      '#default_value' => isset($this->options['more_dates']['dates']['today']) ? $this->options['more_dates']['dates']['today'] : 'long',
    );
    $build['tomorrow'] = array(
      '#type' => 'select',
      '#title' => t('Tomorrow'),
      '#options' => $date_formats,
      '#description' => t('The date format which will be used for field value equal tomorrow.'),
      '#default_value' => isset($this->options['more_dates']['dates']['tomorrow']) ? $this->options['more_dates']['dates']['tomorrow'] : 'long',
    );
    $build['future'] = array(
      '#type' => 'select',
      '#title' => t('Future'),
      '#options' => $date_formats,
      '#description' => t('The date format which will be used for field value more after tomorrow.'),
      '#default_value' => isset($this->options['more_dates']['dates']['future']) ? $this->options['more_dates']['dates']['future'] : 'long',
    );
    $form['date_format']['#options']['more_dates_format'] = t('More date formats');
    $form['more_dates'] = array(
      '#type' => 'fieldset',
      'dates' => $build,
      '#dependency' => array(
        'edit-options-date-format' => array(
          'more_dates_format'),
      ),
    );
  }

/**
 * Render the field.
 *
 */
  function render($values) {
    if ($this->options['date_format'] != 'more_dates_format') {
      return parent::render($values);
    }
    $value = $this->get_value($values);
    if ($value) {
      return more_dates_format_nice_view($value, $this->options['more_dates']['dates']);
    }
  }
}
