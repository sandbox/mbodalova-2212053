
-- INTRODUCTION --

Module More dates provides additional options for displaying date type
fields. You can configure how to show the date for the different values
of field for future, now and past.
Only five settings:
- less than yesterday,
- equally yesterday,
- equally today,
- equally tomorrow,
- greater than tomorrow.
You can choose any format added to the your site -
/admin/config/regional/date-time.
http://php.net/manual/function.date.php


-- REQUIREMENTS --

For use with module view - None.

(Example of use: different ways to show field "date publish" for node.)

For use with fields date type - Date.

(Example of use: different ways to show fields date type in node.
For example - birthday, date for event.)


-- INSTALLATION --

1) Copy the More dates directory to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/modules)



